const http = require("http");


// Creates a variable "port" to store the port number
// 3000, 4000, 5000, 8000 - Usually used for web development
const port = 3000;

const server = http.createServer(function(request, response){

// We will create two endpoint route for "/greeting" "/homepage" and will return a response upon accessing 

// The "url" property refers to the url or the link in the browser( end point)



			if(request.url == "/login"){
				response.writeHead(200,{"Content-type" : "text/plain"});
				response.end("welcome to the login page.");


			}


			else{ // block of code if incase the endpoint or url does not match
		response.writeHead(404, {"Content-Type" : "text/plain"});
		response.end("I'm Sorry the page you are looking for cannot be found.");
	}



	});

server.listen(port);
console.log(`Sever is now accessible at localhost:${port}`);